// Compile with :
// g++ -o specificProducer specificProducer.cpp `pkg-config --libs --cflags cppkafka`

// Basic producer sending numbers on the "test" topic, one per second
// The output message is a JSON
// { "number": xxx }
// The key is the 'number' modulus 3. 
// Every 5 records, force the record to partition 0.

#include <iostream>
#include <ostream>
#include <cppkafka/cppkafka.h>

int main() {
	// Create the config
	cppkafka::Configuration config = {
		{ "bootstrap.servers", "localhost:9092"},
		{ "metadata.max.age.ms", 10000},
	};

	// Create the producer
	cppkafka::Producer producer(config);


	// Send messages
	unsigned int i {0};
	cppkafka::MessageBuilder builder("test");
	cppkafka::MessageBuilder builderZero("test");
	builderZero.partition(0);
	while(true) {
		std::ostringstream ostr;
		ostr << "{ \"number\": " << i << "}";
		std::string message {ostr.str()};
		builder.payload(message);
		builderZero.payload(message);	

		std::cout << message << std::endl;
		
		if (i%5 == 0) {
			producer.produce(builderZero);
		} else {
			producer.produce(builder);
		}	
		
		producer.flush();
		i++;
		std::chrono::milliseconds timespan(1000);
		std::this_thread::sleep_for(timespan);
	}
}
