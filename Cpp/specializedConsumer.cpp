// Compile with :
// g++ -o specializedConsumer specializedConsumer.cpp `pkg-config --libs --cflags cppkafka`

// Basic consumer outputing what is produced on the partition 0 of the topic "test"
// print record value, and partition

#include <iostream>
#include <ostream>
#include <cppkafka/cppkafka.h>

int main() {
	// Create the config
	cppkafka::Configuration config = {
		{ "bootstrap.servers", "localhost:9092" },
		{ "auto.offset.reset", "earliest" },
		{ "group.id", "theGroupOfTheZero" }
	};

	// Create the consumer
	cppkafka::Consumer consumer(config);
        cppkafka::TopicPartition partitionTestZero("test", 0);
	cppkafka::TopicPartitionList topicPartitionList = {partitionTestZero};
        consumer.assign(topicPartitionList);

	// Read messages
	while(true) {
		auto msg = consumer.poll();
		if(msg && ! msg.get_error()) {
			auto message = std::string(msg.get_payload());
			std::cout << message << " read from partition " << msg.get_partition() << std::endl;
		}
	}
}


