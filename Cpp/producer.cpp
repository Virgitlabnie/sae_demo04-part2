// Compile with :
// g++ -o producer producer.cpp `pkg-config --libs --cflags cppkafka`

// Basic producer sending numbers on the "test" topic, one per second
// The output message is a JSON
// { "number": xxx }
// The key is the 'number' modulus 3. 

#include <iostream>
#include <ostream>
#include <cppkafka/cppkafka.h>

int main() {
	// Create the config
	cppkafka::Configuration config = {
		{ "bootstrap.servers", "localhost:9092"},
		{ "metadata.max.age.ms", 10000}
	};

	// Create the producer
	cppkafka::Producer producer(config);


	// Send messages
	unsigned int i {0};
	cppkafka::MessageBuilder builder("test");
	while(true) {
		std::ostringstream ostr;
		ostr << "{ \"number\": " << i++ << "}";
		std::string message {ostr.str()};
		builder.payload(message);	
		
		auto key = std::to_string(i%3);
		std::cout << message << " with key " << key << std::endl;
		builder.key(key);
		
		producer.produce(builder);
		producer.flush();
		std::chrono::milliseconds timespan(1000);
		std::this_thread::sleep_for(timespan);
	}
}
