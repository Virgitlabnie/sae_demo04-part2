// Compile with :
// g++ -o consumer consumer.cpp `pkg-config --libs --cflags cppkafka`

// Basic consumer outputing what is produced on the topic "test"
// print record value, and partition

#include <iostream>
#include <ostream>
#include <cppkafka/cppkafka.h>

int main() {
	// Create the config
	cppkafka::Configuration config = {
		{ "bootstrap.servers", "localhost:9092" },
		{ "auto.offset.reset", "earliest" },
		{ "group.id", "CppGroup" },
		{ "metadata.max.age.ms", 10000}
	};

	// Create the consumer
	cppkafka::Consumer consumer(config);
	consumer.subscribe({"test"});

	// Read messages
	while(true) {
		auto msg = consumer.poll();
		if(msg && ! msg.get_error()) {
			auto message = std::string(msg.get_payload());
			std::cout << message << " with key " << msg.get_key() << " read from partition " << msg.get_partition() << std::endl;
		}
	}
}


