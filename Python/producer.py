from time import sleep
from json import dumps
from kafka import KafkaProducer

producerProperties = { "bootstrap_servers":['localhost:9092'],
                       "metadata_max_age_ms":10000 } 

producer = KafkaProducer(**producerProperties)

i = 0
while True:
   message = {"number":i}
   messageJson = dumps(message)
   messageBytes = messageJson.encode('utf-8')
   repkey = bytes([i % 3])
   futureRecordMetadata = producer.send("test", value=messageBytes, key=repkey)
   print('{message} with key {key} published on partition {partId}'.format(message=message, key=(i%3), partId=futureRecordMetadata.get().partition))
   i+=1
   sleep(1)
