from kafka import KafkaConsumer

consumerProperties = { "bootstrap_servers":['localhost:9092'], 
                       "auto_offset_reset":"earliest",
                       "metadata_max_age_ms":10000,
                       "group_id":"myOwnPrivatePythonGroup" }

consumer = KafkaConsumer(**consumerProperties)
consumer.subscribe("test")

for record in consumer:
   print('{value} read from partition {partId}'.format(value=record.value.decode(), partId=record.partition))
