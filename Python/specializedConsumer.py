from kafka import KafkaConsumer, TopicPartition

consumerProperties = { "bootstrap_servers":['localhost:9092'], 
                       "auto_offset_reset":"earliest",
                       "metadata_max_age_ms":10000 }

consumer = KafkaConsumer(**consumerProperties)
consumer.assign([TopicPartition('test', 0)])

for record in consumer:
   print('{value} read from partition {partId}'.format(value=record.value.decode(), partId=record.partition))
