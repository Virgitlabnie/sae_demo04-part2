package demo04_part2;

import java.time.Duration;
import java.util.Arrays;
import java.util.Collections;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;

public class MySpecializedConsumer {
	
	public static void main(String[] args) {
		new MySpecializedConsumer();
	}

	MySpecializedConsumer() {
		// creates a Kafka consumer with the appropriate configuration
		KafkaConsumer<Integer, String> kafkaConsumer = new KafkaConsumer<>(configureKafkaConsumer());

		// requests partition 0 of the topic 'test'
		TopicPartition topicPartition = new TopicPartition("test", 0);
		kafkaConsumer.assign(Arrays.asList(topicPartition));

		try {
			Duration timeout = Duration.ofMillis(1000);
			ConsumerRecords<Integer, String> records = null;
			while (true) { // I'm a machine, I can work forever

				// queries Kafka every second
				records = kafkaConsumer.poll(timeout);

				// displays the value of each newly polled record
				for (ConsumerRecord<Integer, String> record : records) {
					System.out.println(record.value() + " read from partition " + record.partition());
				}
			}
		} catch (Exception e) {
			System.err.println("something went wrong... " + e.getMessage());
		} finally {
			kafkaConsumer.close();
		}
	}

	/**
	 * Prepares configuration for the Kafka consumer.
	 * 
	 * @return configuration properties for the Kafka consumer
	 */
	private Properties configureKafkaConsumer() {
		Properties consumerProperties = new Properties();

		consumerProperties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		consumerProperties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
				org.apache.kafka.common.serialization.IntegerDeserializer.class);
		consumerProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
				org.apache.kafka.common.serialization.StringDeserializer.class);
		consumerProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest"); // from beginning
		consumerProperties.put(ConsumerConfig.METADATA_MAX_AGE_CONFIG,10000);
		
		return consumerProperties;
	}
}
